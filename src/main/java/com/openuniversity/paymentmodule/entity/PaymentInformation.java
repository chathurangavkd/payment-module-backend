package com.openuniversity.paymentmodule.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity(name = "PAYMENT_INFORMATION")
public class PaymentInformation extends Base {
    String customerId;
    String customerReferenceCode;
    String transactionId;
    String number;
    String cvv;
    String expirationMonth;
    String expirationYear;
}