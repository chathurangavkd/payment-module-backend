package com.openuniversity.paymentmodule.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@MappedSuperclass
public abstract class Base {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String status;
}
