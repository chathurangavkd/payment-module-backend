package com.openuniversity.paymentmodule.service;

import com.openuniversity.paymentmodule.entity.Payment;

import java.util.List;

public interface PaymentService {
    Object createCreditCard(Payment payment);
    Object createCounterPayment(Payment payment);
//    Object createBankDeposit(Payment payment);
    Object initiatePayment(Payment payment);
    List<Object> getPaymentListByPaymentType(String paymentType);
    List<Object> getPaymentListByPaymentStatus(String paymentStatus);
    List<Payment> getPaidApplicationListByUserId(String userId);
    List<Payment> getUnpaidApplicationListByUserId(String userId);
    Payment getApplicationByApplicationId(String applicationId);
    Payment getPendingApplicationByApplicationId(String applicationId);
}
