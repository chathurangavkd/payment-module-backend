package com.openuniversity.paymentmodule.entity;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Entity;

@Data
@Entity(name = "ORDER_INFORMATION")
public class OrderInformation extends Base {
    Double totalAmount;
    String currency;
    Double paidAmount;
    @NaturalId
    String applicationNo;
    String program;
    String accademicYear;
    String studentNic;
    String firstName;
    String lastName;
    String address1;
    String locality;
    String administrativeArea;
    String postalCode;
    String country;
    String email;
    String phoneNumber;
    String billTo;
}