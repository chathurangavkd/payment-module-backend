package com.openuniversity.paymentmodule.controller;

import com.openuniversity.paymentmodule.entity.Payment;
import com.openuniversity.paymentmodule.service.PaymentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PaymentController {

    private PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping(path = "/payment/credit-card/create")
    public Object createCreditCard(@RequestBody Payment payment) {
        return paymentService.createCreditCard(payment);
    }

    @PostMapping(path = "/payment/counter/create/")
    public Object createCounterPayment(@RequestBody Payment payment){
        return paymentService.createCounterPayment(payment);
    }

//    @PostMapping(path = "/payment/bank-deposit/create")
//    public Object createBankDeposit(@RequestBody Payment payment){
//        return paymentService.createBankDeposit(payment);
//    }

    @PostMapping(path = "/payment/initiate")
    public Object initiatePayment(@RequestBody Payment payment){
        return paymentService.initiatePayment(payment);
    }

    @GetMapping(path = "/payment/payment-detail/payment-type/{paymentType}")
    public Object getPaymentListByPaymentType(@PathVariable("paymentType") String paymentType){
        return paymentService.getPaymentListByPaymentType(paymentType);
    }

    @GetMapping(path = "/payment/payment-detail/payment-status/{paymentStatus}")
    public Object getPaymentListByPaymentStatus(@PathVariable("paymentStatus") String paymentStatus){
        return paymentService.getPaymentListByPaymentStatus(paymentStatus);
    }

    @GetMapping(path = "/payment/application-list/unpaid/{userId}")
    public List<Payment> getUnpaidApplicationListByUserId(@PathVariable("userId") String userId){
        return paymentService.getUnpaidApplicationListByUserId(userId);
    }

    @GetMapping(path = "/payment/application-list/paid/{userId}")
    public List<Payment> getPaidApplicationListByUserId(@PathVariable("userId") String userId){
        return paymentService.getPaidApplicationListByUserId(userId);
    }

    @GetMapping(path = "/payment/application/{applicationId}")
    public Payment getApplicationByApplicationId(@PathVariable("applicationId") String applicationId){
        return paymentService.getApplicationByApplicationId(applicationId);
    }

    @GetMapping(path = "/payment/pending-application/{applicationId}")
    public Payment getPendingApplicationByApplicationId(@PathVariable("applicationId") String applicationId){
        return paymentService.getPendingApplicationByApplicationId(applicationId);
    }
}
