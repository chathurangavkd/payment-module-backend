package com.openuniversity.paymentmodule.controller;

import com.openuniversity.paymentmodule.entity.Customer;
import com.openuniversity.paymentmodule.service.CustomerService;
import org.springframework.web.bind.annotation.*;

@RestController
public class CustomerController {
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(path = "/customer/create")
    public Object create(@RequestBody Customer customer) {
        return customerService.create(customer);
    }

    @GetMapping(path = "/customer/get/{customerTokenId}")
    public Object get(@PathVariable("customerTokenId") String customerTokenId) {
        return customerService.get(customerTokenId);
    }
}
