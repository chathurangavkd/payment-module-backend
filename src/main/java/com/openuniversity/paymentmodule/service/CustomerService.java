package com.openuniversity.paymentmodule.service;

import com.openuniversity.paymentmodule.entity.Customer;

public interface CustomerService {
    Object create(Customer customer);
    Object get(String customerTknId);
}
