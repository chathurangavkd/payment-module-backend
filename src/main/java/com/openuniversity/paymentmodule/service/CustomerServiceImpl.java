package com.openuniversity.paymentmodule.service;

import Api.CustomerApi;
import Invokers.ApiClient;
import Model.PostCustomerRequest;
import Model.TmsV2CustomersResponse;
import Model.Tmsv2customersBuyerInformation;
import Model.Tmsv2customersClientReferenceInformation;
import com.cybersource.authsdk.core.MerchantConfig;
import com.openuniversity.paymentmodule.Data.Configuration;
import com.openuniversity.paymentmodule.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class CustomerServiceImpl implements CustomerService{

    private static String responseCode = null;
    private static String status = null;
    private static Properties merchantProp;

    @Override
    public Object create(Customer customer) {
        PostCustomerRequest requestObj = new PostCustomerRequest();

        Tmsv2customersBuyerInformation buyerInformation = new Tmsv2customersBuyerInformation();
        buyerInformation.merchantCustomerID(customer.getBuyerInformation().getMerchantCustomerID());
        buyerInformation.email(customer.getBuyerInformation().getEmail());
        requestObj.buyerInformation(buyerInformation);

        Tmsv2customersClientReferenceInformation clientReferenceInformation = new Tmsv2customersClientReferenceInformation();
        clientReferenceInformation.code(customer.getPaymentInformation().getCustomerReferenceCode());
        requestObj.clientReferenceInformation(clientReferenceInformation);

//
//        List <Tmsv2customersMerchantDefinedInformation> merchantDefinedInformation =  new ArrayList <Tmsv2customersMerchantDefinedInformation>();
//        Tmsv2customersMerchantDefinedInformation merchantDefinedInformation1 = new Tmsv2customersMerchantDefinedInformation();
//        merchantDefinedInformation1.name("data1");
//        merchantDefinedInformation1.value("Your customer data");
//        merchantDefinedInformation.add(merchantDefinedInformation1);
//
//        requestObj.merchantDefinedInformation(merchantDefinedInformation);

        TmsV2CustomersResponse result = null;
        try {
            merchantProp = Configuration.getMerchantDetails();
            ApiClient apiClient = new ApiClient();
            MerchantConfig merchantConfig = new MerchantConfig(merchantProp);
            apiClient.merchantConfig = merchantConfig;

            CustomerApi apiInstance = new CustomerApi(apiClient);
            result = apiInstance.postCustomer(requestObj, null);

            responseCode = apiClient.responseCode;
            status = apiClient.status;
            System.out.println("ResponseCode :" + responseCode);
            System.out.println("ResponseMessage :" + status);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Object get(String customerTknId) {
        String customerTokenId = customerTknId;

        TmsV2CustomersResponse result = null;
        try {
            merchantProp = Configuration.getMerchantDetails();
            ApiClient apiClient = new ApiClient();
            MerchantConfig merchantConfig = new MerchantConfig(merchantProp);
            apiClient.merchantConfig = merchantConfig;

            CustomerApi apiInstance = new CustomerApi(apiClient);
            result = apiInstance.getCustomer(customerTokenId, null);

            responseCode = apiClient.responseCode;
            status = apiClient.status;
            System.out.println("ResponseCode :" + responseCode);
            System.out.println("ResponseMessage :" + status);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
