package com.openuniversity.paymentmodule.entity;


import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Data
@Entity(name = "CUSTOMER")
public class Customer extends Base {

    @OneToOne(cascade = CascadeType.ALL)
    BuyerInformation buyerInformation;

    @OneToOne(cascade = CascadeType.ALL)
    PaymentInformation paymentInformation;

    @OneToOne(cascade = CascadeType.ALL)
    OrderInformation orderInformation;


}
