package com.openuniversity.paymentmodule.entity;
import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity(name = "BUYER_INFORMATION")
public class BuyerInformation extends Base {
    String merchantCustomerID;
    String email;
}
