package com.openuniversity.paymentmodule.repo;

import com.openuniversity.paymentmodule.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PaymentRepo extends JpaRepository<Payment, Long> {
//    @Query("select u from PAYMENT u where u.orderInformation.studentNic = ?1 and u.status = ?2")
//    List<Payment> getApplicationListByUserId(String userId, String status);

//    @Query("select u from PAYMENT u where u.orderInformation.applicationNo= ?1")
//    List<Payment> getApplicationByApplicationId(String applicationId);

    @Query("select u from PAYMENT u where u.orderInformation.applicationNo= ?1 and (u.status = ?2 or u.status = ?3)")
    List<Payment> getPendingApplicationByApplicationId(String applicationId, String status1, String status2);

    List<Payment> getPaymentsByOrderInformation_StudentNicAndStatus(String userId, String status);
    Payment getFirstByOrderInformation_ApplicationNo(String applicationId);
    Payment getFirstByOrderInformation_ApplicationNoAndStatus(String applicationId, String status);

}
