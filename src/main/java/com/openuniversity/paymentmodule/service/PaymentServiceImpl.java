package com.openuniversity.paymentmodule.service;

import com.openuniversity.paymentmodule.Data.Configuration;
import com.openuniversity.paymentmodule.entity.Payment;
import com.openuniversity.paymentmodule.repo.PaymentRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Properties;
import com.cybersource.authsdk.core.MerchantConfig;

import Api.*;
import Invokers.ApiClient;
import Model.*;

@Service
public class PaymentServiceImpl implements PaymentService{

    private PaymentRepo paymentRepo;

    private static String responseCode = null;
    private static String status = null;
    private static Properties merchantProp;
    public static boolean userCapture = false;

    public PaymentServiceImpl(PaymentRepo paymentRepo) {
        this.paymentRepo = paymentRepo;
    }

    @Override
    public Object createCreditCard(Payment payment) {

        CreatePaymentRequest requestObj = new CreatePaymentRequest();

        Ptsv2paymentsClientReferenceInformation clientReferenceInformation = new Ptsv2paymentsClientReferenceInformation();
        clientReferenceInformation.code(payment.getPaymentInformation().getCustomerReferenceCode());
//        clientReferenceInformation.code("TC50171_3");
        requestObj.clientReferenceInformation(clientReferenceInformation);

        Ptsv2paymentsProcessingInformation processingInformation = new Ptsv2paymentsProcessingInformation();
        processingInformation.capture(false);
        if (userCapture) {
            processingInformation.capture(true);
        }

        requestObj.processingInformation(processingInformation);

        Ptsv2paymentsPaymentInformation paymentInformation = new Ptsv2paymentsPaymentInformation();
        Ptsv2paymentsPaymentInformationCard paymentInformationCard = new Ptsv2paymentsPaymentInformationCard();
        paymentInformationCard.number(payment.getPaymentInformation().getNumber());
//        paymentInformationCard.number("4111111111111111");
        paymentInformationCard.expirationMonth(payment.getPaymentInformation().getExpirationMonth());
        paymentInformationCard.expirationYear(payment.getPaymentInformation().getExpirationYear());
//        paymentInformationCard.expirationMonth("12");
//        paymentInformationCard.expirationYear("2031");
        paymentInformation.card(paymentInformationCard);

        requestObj.paymentInformation(paymentInformation);

        Ptsv2paymentsOrderInformation orderInformation = new Ptsv2paymentsOrderInformation();
        Ptsv2paymentsOrderInformationAmountDetails orderInformationAmountDetails = new Ptsv2paymentsOrderInformationAmountDetails();
        orderInformationAmountDetails.totalAmount("102.21");
        orderInformationAmountDetails.currency("USD");
        orderInformation.amountDetails(orderInformationAmountDetails);

        Ptsv2paymentsOrderInformationBillTo orderInformationBillTo = new Ptsv2paymentsOrderInformationBillTo();
        orderInformationBillTo.firstName("John");
        orderInformationBillTo.lastName("Doe");
        orderInformationBillTo.address1("1 Market St");
        orderInformationBillTo.locality("san francisco");
        orderInformationBillTo.administrativeArea("CA");
        orderInformationBillTo.postalCode("94105");
        orderInformationBillTo.country("US");
        orderInformationBillTo.email("test@cybs.com");
        orderInformationBillTo.phoneNumber("4158880000");
//        orderInformationBillTo.firstName(payment.getOrderInformation().getFirstName());
//        orderInformationBillTo.lastName(payment.getOrderInformation().getLastName());
//        orderInformationBillTo.address1(payment.getOrderInformation().getAddress1());
//        orderInformationBillTo.locality(payment.getOrderInformation().getLocality());
//        orderInformationBillTo.administrativeArea(payment.getOrderInformation().getAdministrativeArea());
//        orderInformationBillTo.postalCode(payment.getOrderInformation().getPostalCode());
//        orderInformationBillTo.country(payment.getOrderInformation().getCountry());
//        orderInformationBillTo.email(payment.getOrderInformation().getEmail());
//        orderInformationBillTo.phoneNumber(payment.getOrderInformation().getPhoneNumber());
//        orderInformation.billTo(orderInformationBillTo);

        requestObj.orderInformation(orderInformation);

        PtsV2PaymentsPost201Response result = null;
        try {
            merchantProp = Configuration.getMerchantDetails();
            ApiClient apiClient = new ApiClient();
            MerchantConfig merchantConfig = new MerchantConfig(merchantProp);
            apiClient.merchantConfig = merchantConfig;

            PaymentsApi apiInstance = new PaymentsApi(apiClient);
            result = apiInstance.createPayment(requestObj);

            responseCode = apiClient.responseCode;
            status = apiClient.status;
            if ("201".equals(apiClient.responseCode)){
                Payment payment1 = paymentRepo.getFirstByOrderInformation_ApplicationNo(payment.getOrderInformation().getApplicationNo());
                payment1.getOrderInformation().setPaidAmount(payment.getOrderInformation().getPaidAmount());
                setPaymentStatus(payment1);
                payment1.setPaymentType("CreditCard");
                paymentRepo.save(payment1);
            }
            System.out.println("ResponseCode :" + responseCode);
            System.out.println("ResponseMessage :" + status);
            System.out.println(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
//        CreatePaymentRequest requestObj = new CreatePaymentRequest();
//
//        Ptsv2paymentsClientReferenceInformation clientReferenceInformation = new Ptsv2paymentsClientReferenceInformation();
//        clientReferenceInformation.code("TC50171_3");
//        requestObj.clientReferenceInformation(clientReferenceInformation);
//
//        Ptsv2paymentsProcessingInformation processingInformation = new Ptsv2paymentsProcessingInformation();
//        processingInformation.capture(false);
//        if (userCapture) {
//            processingInformation.capture(true);
//        }
//
//        requestObj.processingInformation(processingInformation);
//
//        Ptsv2paymentsPaymentInformation paymentInformation = new Ptsv2paymentsPaymentInformation();
//        Ptsv2paymentsPaymentInformationCard paymentInformationCard = new Ptsv2paymentsPaymentInformationCard();
//        paymentInformationCard.number(payment.getPaymentInformation().getNumber());
//        paymentInformationCard.expirationMonth(payment.getPaymentInformation().getExpirationMonth());
//        paymentInformationCard.expirationYear(payment.getPaymentInformation().getExpirationYear());
//        paymentInformation.card(paymentInformationCard);
//
//        requestObj.paymentInformation(paymentInformation);
//
//        Ptsv2paymentsOrderInformation orderInformation = new Ptsv2paymentsOrderInformation();
//        Ptsv2paymentsOrderInformationAmountDetails orderInformationAmountDetails = new Ptsv2paymentsOrderInformationAmountDetails();
//        orderInformationAmountDetails.totalAmount(payment.getOrderInformation().getTotalAmount());
//        orderInformationAmountDetails.currency(payment.getOrderInformation().getCurrency());
//        orderInformation.amountDetails(orderInformationAmountDetails);
//
//        Ptsv2paymentsOrderInformationBillTo orderInformationBillTo = new Ptsv2paymentsOrderInformationBillTo();
//        orderInformationBillTo.firstName(payment.getOrderInformation().getFirstName());
//        orderInformationBillTo.lastName(payment.getOrderInformation().getLastName());
//        orderInformationBillTo.address1(payment.getOrderInformation().getAddress1());
//        orderInformationBillTo.locality(payment.getOrderInformation().getLocality());
//        orderInformationBillTo.administrativeArea(payment.getOrderInformation().getAdministrativeArea());
//        orderInformationBillTo.postalCode(payment.getOrderInformation().getPostalCode());
//        orderInformationBillTo.country(payment.getOrderInformation().getCountry());
//        orderInformationBillTo.email(payment.getOrderInformation().getEmail());
//        orderInformationBillTo.phoneNumber(payment.getOrderInformation().getPhoneNumber());
//        orderInformation.billTo(orderInformationBillTo);
//
//        requestObj.orderInformation(orderInformation);
//
//        PtsV2PaymentsPost201Response result = null;
//        try {
//            merchantProp = Configuration.getMerchantDetails();
//            ApiClient apiClient = new ApiClient();
//            MerchantConfig merchantConfig = new MerchantConfig(merchantProp);
//            apiClient.merchantConfig = merchantConfig;
//
//            PaymentsApi apiInstance = new PaymentsApi(apiClient);
//            result = apiInstance.createPayment(requestObj);
//
//            responseCode = apiClient.responseCode;
//            status = apiClient.status;
//            System.out.println("ResponseCode :" + responseCode);
//            System.out.println("ResponseMessage :" + status);
//            System.out.println(result);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result;

    }

    @Override
    public Object createCounterPayment(Payment payment) {
        Payment paymentObj = paymentRepo.getFirstByOrderInformation_ApplicationNo(payment.getOrderInformation().getApplicationNo());
        Double newPaidAmount = paymentObj.getOrderInformation().getPaidAmount() + payment.getOrderInformation().getPaidAmount();
        paymentObj.getOrderInformation().setPaidAmount(newPaidAmount);
        paymentObj.setPaymentType("CounterPayment");
        setPaymentStatus(paymentObj);
        return paymentRepo.save(paymentObj);
    }

//    @Override
//    public Object createBankDeposit(Payment payment) {
//        setPaymentStatus(payment);
//        payment.setPaymentType("BankDeposit");
//        return paymentRepo.save(payment);
//    }

    @Override
    public Object initiatePayment(Payment payment) {
        Double paidAmount = 0.0;
        payment.getOrderInformation().setPaidAmount(paidAmount);
        setPaymentStatus(payment);
        return paymentRepo.save(payment);
    }

    @Override
    public List<Object> getPaymentListByPaymentType(String paymentType) {
        return null;
    }

    @Override
    public List<Object> getPaymentListByPaymentStatus(String paymentStatus) {
        return null;
    }

    @Override
    public List<Payment> getPaidApplicationListByUserId(String userId) {
        return paymentRepo.getPaymentsByOrderInformation_StudentNicAndStatus(userId, "Paid");
    }

    @Override
    public List<Payment> getUnpaidApplicationListByUserId(String userId) {
        return paymentRepo.getPaymentsByOrderInformation_StudentNicAndStatus(userId, "UnPaid");
    }

    @Override
    public Payment getApplicationByApplicationId(String applicationId) {
        return paymentRepo.getFirstByOrderInformation_ApplicationNo(applicationId);
    }

    @Override
    public Payment getPendingApplicationByApplicationId(String applicationId) {
        return paymentRepo.getFirstByOrderInformation_ApplicationNoAndStatus(applicationId, "UnPaid");
    }

    public void setPaymentStatus(Payment payment){
        Double totalAmount = payment.getOrderInformation().getTotalAmount();
        Double paidAmount = payment.getOrderInformation().getPaidAmount();
        if(totalAmount > paidAmount && paidAmount > 0){
            payment.setStatus("PartialPaid");
        }
        else if (totalAmount - paidAmount == 0){
            payment.setStatus("Paid");
        }
        else if (totalAmount > paidAmount && paidAmount == 0){
            payment.setStatus("UnPaid");
        }
    }
}
